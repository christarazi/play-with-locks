#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <threads.h>
#include <time.h>
#include <unistd.h>

// Source: https://gcc.gnu.org/onlinedocs/cpp/Common-Predefined-Macros.html
#define GCC_VERSION \
    (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#define NUM_THREADS 300
#define NUM_ITERATIONS 100000

typedef volatile uint64_t myuint64_t;
static myuint64_t global = 0;
static mtx_t mutex;

struct ThreadData {
#if defined(NOT_ATOMIC)
    mtx_t *mutex;
#endif

    clock_t diff;
};

/*
 * This function behaves differently depending on the mode.
 *
 * In ATOMIC_C11: atomically add / subtract "global" with "val" using C11
 *                primitives.
 * In BEFORE_C11: atomically add / subtract "global" with "val" using a
 *                non-portable compiler-provided function.
 * In NOT_ATOMIC: lock / unlock a mutex to protect the modification of
 *                "global".
 */
static void lock(struct ThreadData const *ptr, int val)
{
#if defined(ATOMIC_C11)
    (void)ptr; // Suppress warning about not using ptr.
    atomic_fetch_add(&global, val);
#elif defined(BEFORE_C11)
    (void)ptr; // Suppress warning about not using ptr.
    __sync_add_and_fetch(&global, val);
#else
    mtx_lock(ptr->mutex);
    global += val;
    mtx_unlock(ptr->mutex);
#endif
}

int work(void *data)
{
    struct ThreadData *p = (struct ThreadData *)data;

    clock_t start, diff;
    start = clock();
    for (size_t i = 0; i < NUM_ITERATIONS; ++i) {
        lock(p, 1);  // Acquiring lock
        // Critical section code goes here.
        lock(p, -1);  // Releasing lock
    }
    diff    = clock() - start;
    diff    = (diff * 1000) / CLOCKS_PER_SEC;
    p->diff = diff;

    return 0;
}

static void create_threads(thrd_t *threads, size_t tlen,
                           struct ThreadData *data)
{
    mtx_init(&mutex, mtx_plain);

    for (size_t i = 0; i < tlen; ++i) {
        struct ThreadData p;
#if defined(NOT_ATOMIC)
        p.mutex = &mutex;
#endif
        p.diff  = 0;
        data[i] = p;

        thrd_create(&threads[i], work, &data[i]);
    }
}

static double aggregate_threads(thrd_t *threads, size_t tlen,
                                struct ThreadData *data)
{
    double total = 0.0;
    for (size_t i = 0; i < tlen; ++i) {
        thrd_join(threads[i], NULL);
        total += data[i].diff;
    }

    mtx_destroy(&mutex);

    return total;
}

int main()
{
    char *msg = NULL;
#if defined(ATOMIC_C11)
    msg = "ATOMIC_C11 (fast)";
#elif defined(BEFORE_C11)
    msg = "BEFORE_C11 (fast)";
#else
    msg = "NOT_ATOMIC (slow)";
#endif
    printf("Compiled with mode: '%s'\n\n", msg);

    thrd_t threads[NUM_THREADS];
    struct ThreadData data[NUM_THREADS];

    printf("Starting %d worker threads with %d locking loops...", NUM_THREADS,
           NUM_ITERATIONS);
    create_threads(threads, ARRAY_SIZE(threads), data);

    double total = aggregate_threads(threads, ARRAY_SIZE(threads), data);
    printf("done\n");

    printf("global = %ld\n", global);
    printf("expect = 0\n");

    printf("total time                        = %lf msecs\n",
           total / CLOCKS_PER_SEC * 1000);
    printf("average execution time per thread = %lf msecs\n",
           total / CLOCKS_PER_SEC / NUM_THREADS * 1000);

    return 0;
}
