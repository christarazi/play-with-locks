# play-with-locks

This repo is a playground for trying to understand the impact of atomic vs.
non-atomic operations on multithreaded application performance. The reason for
this repo is at work we discovered a TLS performance issue when using MySQL
5.7.X with OpenSSL 1.0.2g related to non-atomic code. I wrote this minimal
proof-of-concept to understand the performance impact of such code. See
[Background](#Background) for more about what atomic and non-atomic operations
are. See [corresponding blog
post](https://christarazi.me/blog/impact-locks-and-lockless/) about how we
discovered this issue.

## What play-with-locks does

This program spins up `300` (`NUM_THREADS`) threads. Each thread is executing
`work()` which calls `lock()` with `1` to acquire the "lock" and `-1` to
release the "lock". The critical section of your code is meant to go between
these calls to `lock()`.  We don't perform any operations specifically because
we don't have anything to do. We only want to benchmark how quickly a lock and
unlock happens.  We do this 1 million times (`NUM_ITERATIONS`).  The goal of
this program is to ex cerise the locking mechanism as much as possible and
measure the amount of time each locking mechanism takes.

The `lock()` has 3 different implementations according to the mode set (e.g.
`slow`, `before-c11`, `c11`). This allows us to benchmark the different modes.
The program measures the time it takes for each thread to complete the "work".

Here's the breakdown of what `lock()` does in each mode:

`slow`: locks / unlocks a mutex to perform a `+=` on counter

`before-c11`: atomically increments / decrements a counter using non-portable
compiler-provided function

`c11`: atomically increments / decrements a counter using C11 primitives

In the output of the program, you will see the counter referred to above as
"global". This variable is the "shared resource" that `lock()` is protecting.
During a lock, we increment "global" by 1. During an unlock, we decrement by 1.
When the program is compiled in atomic mode, then instead of locking /
unlocking a mutex to access "global", we simply increment / decrement "global"
directly. In all cases, you should expect to see it equal to `0` in the end.
That means we've properly synchronized all threads accessing this global
variable.

`play-with-locks.c` is a C11 program, meaning it is meant to be compiled
against the C11 standard (`-std=c11`). The reason is because we want to make
use of the builtin support for atomics, which is provided in that standard.
For a POSIX system, this means that we require a version of `gcc` >= 4.9 (or
`clang` >= 3.1) and `glibc` >= 2.28, because they provide support for C11
threads.

*`glibc` isn't explicity required -- just any C library which supports C11
threads.*

# Usage

This program is meant to be recompiled to switch between the different modes.
The modes are:

 - `before-c11` -- pre-C11 atomics (this is just out of curiosity)
 - `c11`        -- C11 atomics
 - `slow`       -- uses mutexes

1) Clone this repo. You will need `bash` and `make`. Also see the above
paragraph for other dependencies.

2) Compile using `make`. You can select the following modes:

 - `before-c11`
 - `c11`
 - `slow`

The default is `slow`.

```bash
$ cd /path/to/repo
$ make <mode>
$ ./play-with-locks
```

3) For the curious, you can view the disassembly by doing:

```bash
$ make disassemble
```

This will show the disassembly for the `lock()` which is what we care about. If
you'd like to see the full disassembly, remove the `|` in the `Makefile` to
`awk`. *Note*: you will need `gawk` version of `awk`. The `mawk` variant does
not understand the regex used.

# Background

Let's take a step back. What is an atomic operation? An *atomic operation* is
an operation that cannot be interrupted while being executed. Technically, the
memory address that the operation is acting on, cannot be read or modified from
until the operation is done.

Atomic operations are at the core of concurrent programming. They synchronize
access to a shared resource across threads.

However, atomic operations have limitations. These operations get compiled to
specific instructions which support only a limited set of data types such as
`int`'s.

With that said, atomic operations are used to build higher-level abstractions
such as mutexes.

> When should I use a mutex vs. an atomic operation?

You use a mutex when you have a critical section of code which acts on a shared
resource among multiple threads. You use the mutex to synchronize access to the
shared resource by locking before starting, and unlocking when done. An example
of a shared resource can be a complex data structure such as a linked list or
an array.

On the other hand, you would use atomic operations when your shared resource is
a simple data type such as `int` or `bool`. This is much faster than wrapping
the modification of the data type (e.g. adding / incrementing) with a mutex
lock / unlock.

Code paths leading to acquiring and releasing mutexes can be very hot
especially in heavily threaded applications. They can easily become bottlenecks
if they are not optimized. In fact, this is what happened with OpenSSL 1.0.2g
and MySQL. OpenSSL had a shared `int` across threads and used a mutex to
protect access to it, *rather* than atomically incrementing / decrementing it.

For more detailed information on how locks are implemented, this is a great
answer on SO that explains concepts like understanding locks, atomicity, memory
barriers, C++11's memory model (which C11 uses), etc., very thoroughly:
https://stackoverflow.com/a/39396999
