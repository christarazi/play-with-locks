CC ?= cc
CFLAGS += -I. -std=c11 -Og -g -Wall -Wextra -Wpedantic -pthread

FLAGS = NOT_ATOMIC  # Default is slow
EXE = play-with-locks

all: $(EXE)

%.o: %.c
	$(CC) -D $(FLAGS) $(CFLAGS) -c -o $@ $<

$(EXE): $(EXE).o
	$(CC) -D $(FLAGS) $(CFLAGS) -o $(EXE) $(EXE).o

slow: FLAGS = NOT_ATOMIC
slow: all

c11: FLAGS = ATOMIC_C11
c11: all

before-c11: FLAGS = BEFORE_C11
before-c11: all

disassemble: all
	@# The '$$' is to escape the single '$' in Makefiles.
	objdump -M intel -d -S ./$(EXE).o | \
		awk '/^[[:xdigit:]]+ <lock>:$$/{flag=1;next}/^[[:xdigit:]]+ <.*>:$$/{flag=0}flag'

.PHONY: clean
clean:
	rm $(EXE) $(EXE).o
